function addToInputField(event) {
    input_fld.text = input_fld.text + event.getSource().id;
}

function clearInput(event) {
    input_fld.text = "";
}

function calculate(event) {
    callRemoteMethod({
        method: "calculate"
        , args: [ { content: input_fld.text } ]
        , onSuccess: function(result) {
            input_fld.text = result.get("content");
        }
        , onError: function(err) {
            var message = "Something went wrong! \n" + err.message;
            input_fld.text = message;
            java.lang.System.out.println(err);
        }
    });
}