package it.pkg;

import com.mjolnirr.lib.component.AbstractApplication;
import com.mjolnirr.lib.component.ComponentContext;
import com.mjolnirr.lib.msg.Communicator;
import com.mjolnirr.lib.msg.HornetCommunicator;

import java.util.ArrayList;

public class Calculator extends AbstractApplication {
    private ComponentContext context;

    @Override
    public void initialize(ComponentContext componentContext) {
        this.context = componentContext;
    }

    public Response calculate(final Request request) throws Exception {
        System.out.println("Context " + request.getContent());

        Communicator communicator = new HornetCommunicator();
        String result = communicator.sendSync(context, "calculatorhelper", "execute", new ArrayList<Object>() {{ add(request.getContent());}}, String.class);

        return new Response(result);
    }
}
