package it.pkg;

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 10/5/13
 * Time: 11:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class Response {
    private String content;

    public Response() {}

    public Response(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
