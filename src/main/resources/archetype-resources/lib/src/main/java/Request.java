#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 10/5/13
 * Time: 11:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class Request {
    private String content;

    public Request() {}

    public Request(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
